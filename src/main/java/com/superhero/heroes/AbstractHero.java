package com.superhero.heroes;

import com.superhero.HeroStatistics;
import com.superhero.TeamType;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
public abstract class AbstractHero {
    private String name;
    private HeroStatistics heroStatistics;
    private TeamType teamType;

    public AbstractHero(String name, HeroStatistics heroStatistics, TeamType teamType) {
        this.name = name;
        this.heroStatistics = heroStatistics;
        this.teamType = teamType;

        updateStats();
    }

    private void updateStats() {
        switch (teamType) {
            case RED:
                heroStatistics.increaseHealth(50);
//                heroStatistics.setHealth(heroStatistics.getHealth() + 50);
                break;
            case BLUE:
                heroStatistics.increaseAttack(50);
//                heroStatistics.setAttack(heroStatistics.getAttack() + 50);
                break;
            case GREEN:
                heroStatistics.increaseDefence(50);
//                heroStatistics.setDefence(heroStatistics.getDefence() + 50);
                break;
        }
    }

    public abstract int getPower();


}
