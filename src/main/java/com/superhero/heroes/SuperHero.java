package com.superhero.heroes;

import com.superhero.HeroStatistics;
import com.superhero.TeamType;
import com.superhero.heroes.AbstractHero;

public class SuperHero extends AbstractHero {
    SuperHero(String name, HeroStatistics heroStatistics, TeamType teamType) {
        super(name, heroStatistics, teamType);
    }

    public int getPower() {
        int health = getHeroStatistics().getHealth();
        int attack = getHeroStatistics().getAttack();
        int defence = getHeroStatistics().getDefence();

        return (defence + attack) * health;
    }
}
