package com.superhero.heroes;

import com.superhero.HeroStatistics;
import com.superhero.TeamType;
import com.superhero.heroes.AbstractHero;
import com.superhero.heroes.Villain;

// fabryka abstrakcyjna
public abstract class HeroCreator {

    // metody stworzeniowe, metody factory
    public static AbstractHero createVillain() {
        return new Villain("Marianek", new HeroStatistics(150, 50, 50), TeamType.RED);
    }

    public static AbstractHero createSzeregowyVillain(String name) {
        return new Villain(name, new HeroStatistics(50, 50, 50), TeamType.RED);
    }

    public static AbstractHero createSuperHero(String name) {
        return new SuperHero(name, new HeroStatistics(150, 150, 25), TeamType.GREEN);
    }
}
