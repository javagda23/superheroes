package com.superhero;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data // @RequiredArgsConstructor
@AllArgsConstructor
public class HeroStatistics {
    private int health;
    private int attack;
    private int defence;

    public void increaseHealth(int amount) {
        health += amount;
    }

    public void increaseAttack(int amount) {
        attack += amount;
    }

    public void increaseDefence(int amount) {
        defence += amount;
    }
}
