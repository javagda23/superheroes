package com.superhero;

import com.superhero.heroes.AbstractHero;
import com.superhero.heroes.HeroCreator;
import com.superhero.heroes.Villain;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        AbstractHero villain = HeroCreator.createVillain();

        List<AbstractHero> heroes = new ArrayList<AbstractHero>();
        for (int i = 0; i < 100; i++) {
            heroes.add(HeroCreator.createSzeregowyVillain("janusz-" + i));
        }

        Villain v = new Villain("a", new HeroStatistics(1, 1, 1), null);
    }
}
